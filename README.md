# Yape Repo

# Listado de Actividades
- Revisar la documentacion del servicio [Restful booker](https://restful-booker.herokuapp.com/apidoc/index.html)
- Validar que sea una conexión estable
- Crear un Collection en Postman para probar las distintas combinaciones
- Elegir las herramientas para la automatizacion
- Comenzar con el desarrollo

# Herramientas utilizadas
- IntelliJ
- Java
- TestNG
- Rest Assured

# Tests Cases:
- Create token - Happy Path
- Health Check - Happy Path
- Get booking ids - Happy Path
- Get booking with id - Happy Path
- Create booking - Happy Path
- Delete booking - Happy Path
- Update Booking - Happy Path
- Partial Update of booking - Happy Path
- Get booking by name - Happy Path
- Create token - Bad credentials- Unhappy Path
- Get booking with id - Unhappy Path

*Para algunas pruebas, se muestra informacion del usuario en la URL, puede ser peligroso
