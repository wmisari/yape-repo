package yape.endpoints;

import io.restassured.response.Response;

import java.io.File;

import static io.restassured.RestAssured.given;

public class AuthClass extends BaseAPI{
    File goodCredentials = new File("src/test/java/yape/models/GoodCredentials.json");
    File badCredentials = new File("src/test/java/yape/models/BadCredentials.json");
    String auth_endpoint = baseURL + "auth/";

    public Response createToken(){
        return given().contentType("application/json").body(goodCredentials).when().post(auth_endpoint);
    }

    public Response createToken_credential_error(){
        return given().contentType("application/json").body(badCredentials).when().post(auth_endpoint);
    }
}
