package yape.endpoints;

import io.restassured.response.Response;
import java.io.File;
import static io.restassured.RestAssured.given;

public class BookingClass extends BaseAPI{

    static File updateUser = new File("src/test/java/yape/models/UpdateUser.json");
    static File partialUser = new File("src/test/java/yape/models/PartialUpdate.json");


    static String booking_endpoint = baseURL + "booking/";

    public Response getBookingIds() {
        return given().get(booking_endpoint);
    }

    public Response getBookingIds_byName(String firstname, String lastname) {
        String booking_byName_endpoint = baseURL + "booking?firstname="+firstname+"&lastname="+lastname;
        return given().get(booking_byName_endpoint);
    }

    public Response getBooking(String id, String mediaType) {
        return given().header("Accept", mediaType).get(booking_endpoint + id);
    }

    public Response createBooking(File file) {
        return given()
                .contentType("application/json")
                .body(file)
                .when()
                .post(booking_endpoint);
    }

    public Response deleteBooking(String id, String token) {
        return given()
                .header("Cookie", "token=" + token)
                .delete(booking_endpoint + id);
    }

    public Response updateBooking(String id, String token) {
        return given()
                .header("Cookie", "token=" + token)
                .contentType("application/json")
                .body(updateUser)
                .put(booking_endpoint + id);
    }

    public Response partialUpdateBooking(String id, String token) {
        return given()
                .header("Cookie", "token=" + token)
                .contentType("application/json")
                .body(partialUser)
                .patch(booking_endpoint + id);
    }
}
