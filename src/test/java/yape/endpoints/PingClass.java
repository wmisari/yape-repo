package yape.endpoints;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class PingClass extends BaseAPI{
    static String ping_endpoint = baseURL + "ping/";

    public Response healthCheck() {
        return given()
                .get(ping_endpoint);
    }
}
