package yape.tests;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import yape.endpoints.AuthClass;
import yape.endpoints.BookingClass;
import yape.endpoints.PingClass;

import java.io.File;

public class RestTests {

    AuthClass authClass = new AuthClass();
    BookingClass bookingClass = new BookingClass();
    PingClass pingClass = new PingClass();
    static File newUser = new File("src/test/java/yape/models/NewBooking.json");
    static File secondUser = new File("src/test/java/yape/models/NewBooking.json");

    @Test
    public void createToken_StatusOk() {
        Response response = authClass.createToken();
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void healthCheck_StatusOk() {
        Response response = pingClass.healthCheck();
        Assert.assertEquals(response.getStatusCode(), 201);
    }

    @Test
    public void getBookingIds_StatusOk() {
        Response response = bookingClass.getBookingIds();
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void getBooking_byName() {
        Response createResponse = bookingClass.createBooking(secondUser);
        Assert.assertEquals(createResponse.getStatusCode(), 200);
        String newId = createResponse.jsonPath().getString("bookingid");
        Response response = bookingClass.getBookingIds_byName("Wendy","Misari");
        Assert.assertNotEquals(response.jsonPath().getString("bookingid"), null);
        //delete id to reuse
        Response authResponse = authClass.createToken();
        Response deleteResponse = bookingClass.deleteBooking(newId, authResponse.jsonPath().getString("token"));
        Assert.assertEquals(deleteResponse.getStatusCode(), 201);
    }

    @Test
    public void getBooking_byId_StatusOk() {
        Response createResponse = bookingClass.createBooking(newUser);
        Assert.assertEquals(createResponse.getStatusCode(), 200);
        String id = createResponse.jsonPath().getString("bookingid");
        Response response = bookingClass.getBooking(id, "application/json");
        Assert.assertEquals(response.getStatusCode(), 200);
        String Name = response.jsonPath().getString("firstname");
        Assert.assertEquals( Name, "Wendy");
    }

    @Test
    public void createBooking() {
        Response response = bookingClass.createBooking(newUser);
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void deleteBooking() {
        Response bookingResponse = bookingClass.createBooking(newUser);
        Response authResponse = authClass.createToken();
        Response response = bookingClass.deleteBooking(bookingResponse.jsonPath().getString("bookingid"), authResponse.jsonPath().getString("token"));
        Assert.assertEquals(response.getStatusCode(), 201);
    }

    @Test
    public void updateBooking() {
        Response bookingResponse = bookingClass.createBooking(newUser);
        Response authResponse = authClass.createToken();
        String id = bookingResponse.jsonPath().getString("bookingid");
        String token = authResponse.jsonPath().getString("token");
        Response response = bookingClass.updateBooking(id, token);
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void partialUpdateBooking() {
        Response bookingResponse = bookingClass.createBooking(newUser);
        Response authResponse = authClass.createToken();
        String id = bookingResponse.jsonPath().getString("bookingid");
        String token = authResponse.jsonPath().getString("token");
        Response response = bookingClass.partialUpdateBooking(id, token);
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void createToken_BadCredentials() {
        Response response = authClass.createToken_credential_error();
        Assert.assertEquals(response.getStatusCode(), 200);
        String Message = response.jsonPath().getString("reason");
        Assert.assertEquals(Message, "Bad credentials");
    }

    @Test
    public void getBooking_byId_NotFound() {
        Response response = bookingClass.getBooking("999", "application/json");
        Assert.assertEquals(response.getStatusCode(), 404);
    }
}
